package performance

import (
	"fmt"
	"net/http"

	"chainmaker.org/chainmaker/performance/util"
	"github.com/gin-gonic/gin"
)

var (
	perfConfigPort uint16 = 9527
)

func startConfigListener(listenerPort uint16) {
	if listenerPort > 0 {
		perfConfigPort = listenerPort
	}
	go util.SafeGoroutine(runServer, nil)
}

func runServer() error {
	router := gin.Default()
	config := router.Group("/perfconfig")
	config.GET("/allconfigs", getAllConfigs)
	config.POST("/update", updateConfig)
	return router.Run(fmt.Sprintf(":%d", perfConfigPort))
}

// getAllConfigs 向客户端返回全部配置结果
func getAllConfigs(c *gin.Context) {
	c.YAML(http.StatusOK, perfConfig)
}

// updateConfig 更新配置
func updateConfig(c *gin.Context) {
	err := c.BindYAML(&perfConfig)
	if err != nil {
		c.String(http.StatusBadRequest, "update config err: %s", err.Error())
		return
	}
	c.YAML(http.StatusOK, perfConfig)
}
