package model

import "time"

type SampleTransaction struct {
	TxID      string    `gorm:"type:char(64);primarykey"`
	OccurTime time.Time `gorm:"index"`
	Source    int
}

func init() {
	registerModel(&SampleTransaction{})
}
