package model

import (
	_ "gorm.io/gorm"
)

var AllModels []interface{}

func registerModel(model interface{}) {
	AllModels = append(AllModels, model)
}
