package performance

type PerformanceConfig struct {
	MeasureAll        bool `yaml:"measure_all"`
	SampleTransaction bool `yaml:"sample_transaction"`
	SampleRate        int  `yaml:"sample_rate"`
}

var perfConfig PerformanceConfig

func MeasureAll() bool {
	return perfConfig.MeasureAll
}

func SampleTransaction() bool {
	return perfConfig.MeasureAll && perfConfig.SampleTransaction
}

func SampleRate() int {
	return perfConfig.SampleRate
}
