package util

import "fmt"

func HandlePanic() {
	if err := recover(); err != nil {
		fmt.Printf("[Performance] got panic: %+v", err)
	}
}

func SafeGoroutine(f func() error, sig chan<- error) {
	defer HandlePanic()
	err := f()
	if sig != nil {
		sig <- err
	}
}
