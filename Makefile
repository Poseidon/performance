perfConfigPort=9527

allconfigs:
	@curl localhost:${perfConfigPort}/perfconfig/allconfigs
update:
	@curl -XPOST \
	--data-binary @performance_config.yml \
	localhost:${perfConfigPort}/perfconfig/update