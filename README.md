# Performance

用于性能指标的记录

# 用法

- 数据定义

  - 在 chainmaker.org/chainmaker/performance/model 目录下，创建想要统计的性能指标对应的结构体。该结构体应该包含一个主键 primarykey，如 ID；也可以根据需要，将其它字段定义为主键。  
  参考: [GORM指南](https://gorm.io/zh_CN/docs/index.html)
    ```go
    package model

    import "time"

    type SampleTransaction struct {
        TxID      string    `gorm:"type:char(64);primarykey"` // 主键
        OccurTime time.Time `gorm:"index"`      // 在 OccurTime 上建立索引
        Source    int
    }

    func init() {
        registerModel(&SampleTransaction{}) // 将新添加的结构体注册一下，这样调用 performance.InitWithDNS(dns, listenerPort) 的时候，会在数据库中自动创建该结构体对应的 table
    }
    ```

  - 如果结构体对应的table在数据库中不存在，performance 会自动根据结构体的名称在 mysql 数据库中创建对应的table。SampleTransaction 结构体创建出来的表格及其格式如下

    ```bash
    mysql> show tables;
    +----------------------------------+
    | Tables_in_chainmaker_performance |
    +----------------------------------+
    | sample_transactions                     |
    +----------------------------------+
    1 row in set (0.00 sec)

    mysql> show create table sample_transactions \G
    *************************** 1. row ***************************
        Table: sample_transactions
    Create Table: CREATE TABLE `sample_transactions` (
    `tx_id` char(64) NOT NULL,
    `occur_time` datetime(3) DEFAULT NULL,
    `source` bigint DEFAULT NULL,
    PRIMARY KEY (`tx_id`),
    KEY `idx_sample_transactions_occur_time` (`occur_time`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
    1 row in set (0.01 sec)

    mysql> desc sample_transactions;
    +------------+-------------+------+-----+---------+-------+
    | Field      | Type        | Null | Key | Default | Extra |
    +------------+-------------+------+-----+---------+-------+
    | tx_id      | char(64)    | NO   | PRI | NULL    |       |
    | occur_time | datetime(3) | YES  | MUL | NULL    |       |
    | source     | bigint      | YES  |     | NULL    |       |
    +------------+-------------+------+-----+---------+-------+
    3 rows in set (0.01 sec)
    ```

- 在 chainmaker-go 中使用 performance

  - 在项目中引入 performance:

    ```bash
    go get -u chainmaker.org/chainmaker/performance
    ```

  - 在项目启动的过程中，使用 mysql 的地址对 `performance` 包进行初始化

    ```go
    import (
        ......
        "chainmaker.org/chainmaker/performance"
    )

    func main() {
        ......
        dns := "chainmaker:chainmaker@tcp(127.0.0.1:3306)/chainmaker_performance?charset=utf8mb4&parseTime=True&loc=Local"
        listenerPort := 9527 // performance 框架的后门端口，用于动态更新配置，下文`Update1`有详细介绍
        err := performance.InitWithDNS(dns, listenerPort)
        if err != nil {
            // 初始化 performance 失败， do something
        }
        ......
    }
    ```

  - 在 chainmaker-go 的主程序中，在关注的地方，将性能测量数据写入数据库

    ```go
    // file: txpool-normal/normal_tx_pool.go
    import (
        ......
        "chainmaker.org/chainmaker/performance"
        "chainmaker.org/chainmaker/performance/model"
    )

    func (pool *normalPool) AddTx(tx *commonPb.SampleTransaction, source protocol.TxSource) error {
        ......
        if performance.SampleTransaction() { // 这里是 performance 的控制开关，可以通过 listenerPort 来动态配置SampleTransaction()的返回值，详见下文 `Update1`
            transaction := &model.SampleTransaction{
                TxID:      tx.Payload.GetTxId(),
                OccurTime: time.Now(),
                Source:    int(source),
            }
            performance.Record(transaction, nil)    // 这里会启动一个 goroutine，异步执行写入数据库
	    }

        ......
        return nil
    }
    ```

  - 写入数据库的执行方式是异步的，会启动一个 goroutine 执行写入数据库的工作。如果 caller 关注写入数据库是否成功，则可以给 Record() 接口的第二个参数传入 `signal chan error`，然后等待 `signal` 返回结果

    ```go
    // file: txpool-normal/normal_tx_pool.go
    import (
        ......
        "chainmaker.org/chainmaker/performance"
        "chainmaker.org/chainmaker/performance/model"
    )

    func (pool *normalPool) AddTx(tx *commonPb.SampleTransaction, source protocol.TxSource) error {
        ......
        if performance.SampleTransaction() {
            transaction := &model.SampleTransaction{
                TxID:      tx.Payload.GetTxId(),
                OccurTime: time.Now(),
                Source:    int(source),
            }
            signal := make(chan error, 1)
            performance.Record(transaction, signal)
            err := <- signal
            if err != nil {
                // 写入数据库出现错误， do something
            }
        }

        ......
        return nil
    }
    ```
---
# 开发流程演示
## 在 chainmaker-go 中记录 transaction 发生的时刻

### performance 代码开发
- 将 `chainmaker.org/chainmaker/performance` 拉取到本机，在 `model` 目录下添加文件 `sample_transaction.go` （这里的 `sample_*` 表示示例，实际开发过程中请定义自己需要的数据结构）,内容如下：
    ```bash
    $ pwd
    ....../performance/model

    $ tree .
    .
    ├── sample_transaction.go
    └── vars.go

    0 directories, 2 files

    $ cat sample_transaction.go 
    package model

    import "time"

    type SampleTransaction struct {
            TxID      string    `gorm:"type:char(64);primarykey"`
            OccurTime time.Time `gorm:"index"`
            Source    int
    }

    func init() {
            registerModel(&SampleTransaction{})
    }
    ```
- 在 `performance` 根目录下的 `performance_test.go` 中，编写测试用例，然后使用 `go test -run=SampleTransaction` 进行测试，看看是否能够成功写入数据库。
    ```go
    func TestSampleTransaction(t *testing.T) {
        transaction := &model.SampleTransaction{
            TxID:      strconv.Itoa(int(time.Now().UnixNano())),
            OccurTime: time.Now().Add(-time.Hour * 13),
            Source:    1,
        }
        Record(transaction, signal)
        testError(t, signal, false)

        transaction.TxID = strconv.Itoa(int(time.Now().UnixNano()))
        transaction.OccurTime = time.Now()
        Record(transaction, signal)
        testError(t, signal, false)

        Record(transaction, signal)
        testError(t, signal, true) // here we are supposed to get an error

    }
    ```
- `performance_test.go` 文件中的 dns 变量，应该修改成自己提供的 mysql 的dns。如果要在本机启动数据库进行开发测试，可以通过如下方式，使用 docker 快速在本机部署一个 mysql 数据库。  
在不同平台安装 docker，可以参考 [Docker安装](https://www.runoob.com/docker/ubuntu-docker-install.html)
    ```bash
    $ MYSQL=mysql:8.0.31
    $ docker run --name mysql8 -d \
        -e MYSQL_ROOT_PASSWORD=root \
        -e MYSQL_USER=chainmaker \
        -e MYSQL_PASSWORD=chainmaker \
        -e MYSQL_DATABASE=chainmaker_performance \
        -v mysql8-data:/var/lib/mysql \
        -p 3306:3306 \
        ${MYSQL}
    ```
    - 使用如下命令进入刚刚启动的 docker 容器，然后可以在容器中通过命令行的方式连接 mysql，查看数据存储情况。
        ```bash
        $ docker exec -it mysql8 bash
        # 此时已经进入 mysql 容器
        bash-4.4$ mysql -h 127.0.0.1 -P 3306 -u chainmaker -p # 登录数据库
        Enter password: 
        Welcome to the MySQL monitor.  Commands end with ; or \g.
        Your MySQL connection id is 12
        Server version: 8.0.31 MySQL Community Server - GPL

        Copyright (c) 2000, 2022, Oracle and/or its affiliates.

        Oracle is a registered trademark of Oracle Corporation and/or its
        affiliates. Other names may be trademarks of their respective
        owners.

        Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

        mysql>
        ```

### chainmaker-go 代码开发
- 在 `chainmaker-go/config/config_tpl/chainmaker.tpl` 文件的末尾，添加如下配置内容。这些配置内容默认是被注释掉的。
    ```yml
    #plugin:
    #  # Configs for performance measure
    #  performance_measure:
    #    # mysql address, format: 'user:passwd@tcp(ip:port)/database_name?someconfig'
    #    dns: chainmaker:chainmaker@tcp(127.0.0.1:3306)/chainmaker_performance?charset=utf8mb4&parseTime=True&loc=Local
    #    # the port of performance config listener, we use it to dynamically update performance config
    #    perf_config_port: 9527
    ```
    参考 chainmaker 官方文档 [通过命令行体验链](https://docs.chainmaker.org.cn/v2.3.0/html/quickstart/%E9%80%9A%E8%BF%87%E5%91%BD%E4%BB%A4%E8%A1%8C%E4%BD%93%E9%AA%8C%E9%93%BE.html) 搭建集群的时候，在生成的 release 目录下的各个节点中，应当将`chainmaker.yml`文件末尾的这些注释解除，并将 dns 修改成自己提供的 msyql 的 dns; 如果基于同一份代码生成多个node，且各个node在同一台机器上部署运行，那么需要修改各个node的`perf_config_port`为不同的值，避免端口冲突。如下所示：
    ```yaml
    plugin:
    # Configs for performance measure
    performance_measure:
        # mysql address, format: 'user:passwd@tcp(ip:port)/database_name?someconfig'
        dns: chainmaker:chainmaker@tcp(127.0.0.1:3306)/chainmaker_performance?charset=utf8mb4&parseTime=True&loc=Local
        # the port of performance config listener, we use it to dynamically update performance config
        perf_config_port: 9527
    ```
- 对应于上述的 `plugin` 配置项，在 `git.chainmaker.org.cn/chainmaker/localconf` 仓库的 `types.go` 文件中，新添加了如下 `Plugin` 结构:
    ```go
    type CMConfig struct {
        ......
        Plugin          Plugin          `mapstructure:"plugin"`
    }
    type Plugin struct {
        PerformanceMeasure PerformanceMeasure `mapstructure:"performance_measure"`
    }

    type PerformanceMeasure struct {
        DNS            string `mapstructure:"dns"`
        PerfConfigPort uint16 `mapstructure:"perf_config_port"`
    }
    ```
    `chainmaker-go` 引用了 `localconf` 包，并在 `chainmaker-go/main/cmd/cli_start.go` 代码中的 `initLocalConfig(cmd)` 函数中初始化了 `localconf.ChainMakerConfig` 这个包级别的导出变量。如果在 `chainmaker.yml` 中正确配置了 `plugin` 项目，则该配置项会被解析成 `localconf.ChainMakerConfig.Plugin` 这个字段。
    ```go
    func StartCMD() *cobra.Command {
        startCmd := &cobra.Command{
            ......
            RunE: func(cmd *cobra.Command, _ []string) error {
                initLocalConfig(cmd)
                ......
                return nil
            },
        }
        ......
        return startCmd
    }
    ```
- 在 `chainmaker-go/main/cmd/cli_start.go:mainStart()` 函数中，添加如下代码，对 `performance` 包进行初始化：
    ```go
    package cmd

    ......

    func mainStart() {
        if localconf.ChainMakerConfig.DebugConfig.IsTraceMemoryUsage {
            traceMemoryUsage()
        }

        if len(localconf.ChainMakerConfig.Plugin.PerformanceMeasure.DNS) > 0 { // 这里初始化 performance 包; 如果注释掉 chainmaker.yml 中的 plugin 配置项，则不会对 performance 包进行初始化。
            dns := localconf.ChainMakerConfig.Plugin.PerformanceMeasure.DNS
            listenerPort := localconf.ChainMakerConfig.Plugin.PerformanceMeasure.PerfConfigPort
            err := performance.InitWithDNS(dns, listenerPort)
            if err != nil {
                log.Errorf("performance.InitWithDNS(%s, %d) err: %s", dns, listenerPort, err.Error())
            } else {
                log.Infof("performance package connected to mysql")
            }
        }

        // init chainmaker server
        chainMakerServer := blockchain.NewChainMakerServer()
        ......
    }
    ```
    至此，`performance` 就成功 connect 到了 mysql。后续可以使用 `performance.Record()` 接口将数据记录到数据库。如果 `performance` 没有成功 connect 到 mysql，则 `performance.Record()` 会向不为 nil 的 sig 参数传入一个 error，提示 `"[performance] database not connected"`
- 记录交易发生的时刻  
    交易发生的逻辑在 `git.chainmaker.org.cn/chainmaker/txpool-normal` 仓库的 `normal_tx_pool.go:func (pool *normalPool) AddTx()` 函数中，该仓库作为 `package` 被 `chainmaker-go` 引用。  
    我们在 `AddTx()` 函数的合适位置，添加记录 `SampleTransaction` 信息的代码：
    ```go
    // file: txpool-normal/normal_tx_pool.go
    import (
        ......
        "chainmaker.org/chainmaker/performance"
        "chainmaker.org/chainmaker/performance/model"
    )

    func (pool *normalPool) AddTx(tx *commonPb.SampleTransaction, source protocol.TxSource) error {
        ......
        if performance.SampleTransaction() { // chainmaker-go 刚开始启动的时候，performance.SampleTransaction() 为 false, 需要通过后门更改配置来开启测量工作，详见下文 Update1
            transaction := &model.SampleTransaction{
                TxID:      tx.Payload.GetTxId(),
                OccurTime: time.Now(),
                Source:    int(source),
            }
            performance.Record(transaction, nil)
        }

        ......
        return nil
    }
    ```
    此时的 `performance` 包已经在 `chainmaker-go:mainStart()` 函数中被初始化了，所以 `SampleTransaction` 可以被正常记录到数据库(需要通过后门开启测量工作，详见下文 `Update1`)。

- 本机运行 `chainmaker-go`，查看数据是否记录到 mysql
  - 在 `chainmaker-go/go.mod` 中，使用 `replace` 命令将尚未提交到代码仓库的`package`重定向到本地的文件目录。`localconf` 仓库一般只需要第一次开发的时候进行修改(添加`Plugin`结构的定义)，后续的开发，一般不需要 `replace localconf 包`。
    ```go
    module chainmaker.org/chainmaker-go

    go 1.16

    require (
        ......
    )

    replace chainmaker.org/chainmaker/localconf/v2 => ../localconf

    replace chainmaker.org/chainmaker/txpool-normal/v2 => ../txpool-normal

    replace chainmaker.org/chainmaker/performance => ../performance
    ```
  - 参考官方文档 [通过命令行体验链](https://docs.chainmaker.org.cn/v2.3.0/html/quickstart/%E9%80%9A%E8%BF%87%E5%91%BD%E4%BB%A4%E8%A1%8C%E4%BD%93%E9%AA%8C%E9%93%BE.html#permissionedwithcert)，创建本地单机单节点集群
    ```bash
    $ cd chainmaker-go/scripts

    $ ./prepare.sh 1 1 # 这里的两个参数都是 1，是为了部署单节点、单链，方便调试管理
    begin check params...
    begin generate certs, cnt: 1
    input consensus type (0-SOLO,1-TBFT(default),3-MAXBFT,4-RAFT): 0 # 这里的共识协议选择SOLO单节点协议，主要是为了方便测试，其它项目都直接回车选择默认配置项即可
    input log level (DEBUG|INFO(default)|WARN|ERROR): 
    enable vm go (YES|NO(default))
    config node total 1
    begin generate node1 config...
    begin node1 chain1 cert config...
    begin node1 trust config...

    $ tree ../build -L 3
    ../build
    ├── config
    │   └── node1
    │       ├── certs
    │       ├── chainconfig
    │       ├── chainmaker.yml
    │       └── log.yml
    ├── crypto-config
    │   └── wx-org.chainmaker.org
    │       ├── ca
    │       ├── node
    │       └── user
    ├── crypto_config.yml
    └── pkcs11_keys.yml

    9 directories, 4 files
    ```

    在上述命令生成的 `chainmaker-go/build/config/node1/chainmaker.yml` 文件中，手动解除 `plugin` 配置项的注释，并将 `dns` 设置成自己提供的 mysql 地址 -- 本例使用本机部署的 mysql:
    ```yml
    plugin:
      # Configs for performance measure
      performance_measure:
        # mysql address, format: 'user:passwd@tcp(ip:port)/database_name?someconfig'
        dns: chainmaker:chainmaker@tcp(127.0.0.1:3306)/chainmaker_performance?charset=utf8mb4&parseTime=True&loc=Local
        # the port of performance config listener, we use it to dynamically update performance config
        perf_config_port: 9527
    ```

  - 编译项目
    ```bash
    $ cd chainmaker-go/scripts

    $ ./build_release.sh

    $ tree ../build/release/
    ../build/release/
    ├── chainmaker-v2.3.0_alpha-wx-org.chainmaker.org-20221021155924-x86_64.tar.gz
    └── crypto-config-20221021155924.tar.gz

    0 directories, 2 files
    ```
  - 启动节点  
    启动节点前，先将mysql中的 `sample_transactions` 表格删除 -- 该表格是在上述执行 `performance` 包中的 `TestSampleTransaction()` 时创建的:
    ```bash
    mysql> show databases; -- 查看有哪些数据库
    +------------------------+
    | Database               |
    +------------------------+
    | chainmaker_performance |
    | information_schema     |
    | performance_schema     |
    +------------------------+
    3 rows in set (0.01 sec)

    mysql> use chainmaker_performance; -- 切换数据库
    Reading table information for completion of table and column names
    You can turn off this feature to get a quicker startup with -A

    Database changed
    mysql> show tables; -- 查看当前数据库下有哪些表格
    +----------------------------------+
    | Tables_in_chainmaker_performance |
    +----------------------------------+
    | sample_transactions                     |
    +----------------------------------+
    1 row in set (0.01 sec)

    mysql> drop table sample_transactions; -- 删除表格
    Query OK, 0 rows affected (0.03 sec)

    mysql> 
    ```

    然后再启动节点：
    ```bash
    $ ./cluster_quick_start.sh normal
    ```
    可以看到，mysql 中自动创建了 table:
    ```bash
    mysql> show tables;
    +----------------------------------+
    | Tables_in_chainmaker_performance |
    +----------------------------------+
    | sample_transactions                     |
    +----------------------------------+
    1 row in set (0.00 sec)

    mysql> show create table sample_transactions \G
    *************************** 1. row ***************************
        Table: sample_transactions
    Create Table: CREATE TABLE `sample_transactions` (
    `tx_id` char(64) NOT NULL,
    `occur_time` datetime(3) DEFAULT NULL,
    `source` bigint DEFAULT NULL,
    PRIMARY KEY (`tx_id`),
    KEY `idx_transactions_occur_time` (`occur_time`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
    1 row in set (0.00 sec)

    mysql> desc sample_transactions;
    +------------+-------------+------+-----+---------+-------+
    | Field      | Type        | Null | Key | Default | Extra |
    +------------+-------------+------+-----+---------+-------+
    | tx_id      | char(64)    | NO   | PRI | NULL    |       |
    | occur_time | datetime(3) | YES  | MUL | NULL    |       |
    | source     | bigint      | YES  |     | NULL    |       |
    +------------+-------------+------+-----+---------+-------+
    3 rows in set (0.00 sec)
    ```
  - 使用 cmc 命令行创建 wasm 合约
    ```bash
    # 编译cmc
    $ cd $WORKDIR/chainmaker-go/tools/cmc
    $ go build
    # 配置测试数据
    $ cp -rf ../../build/crypto-config ../../tools/cmc/testdata/ # 使用chainmaker-cryptogen生成的测试链的证书

    # 创建 wasm 合约
    $ ./cmc client contract user create \
    --contract-name=fact \
    --runtime-type=WASMER \
    --byte-code-path=./testdata/claim-wasm-demo/rust-fact-2.0.0.wasm \
    --version=1.0 \
    --sdk-conf-path=./testdata/sdk_config_solo.yml \ # 注意这里使用的是 sdk_config_solo.yml
    --admin-key-file-paths=./testdata/crypto-config/wx-org.chainmaker.org/user/admin1/admin1.sign.key \
    --admin-crt-file-paths=./testdata/crypto-config/wx-org.chainmaker.org/user/admin1/admin1.sign.crt \
    --sync-result=true \
    --params="{}"

    # 以下为返回结果
    {
    "contract_result": {
        "gas_used": 1376,
        "result": {
        "address": "c867f29fbc90b619206c7aef0ba4a2efb30ab9f6",
        "creator": {
            "member_id": "client1.sign.wx-org.chainmaker.org",
            "member_info": "uBq8aZsgQ3gY8di9ohOBfWjTNu24S8mta9yS1tVq+rg=",
            "member_type": 1,
            "org_id": "wx-org.chainmaker.org",
            "role": "CLIENT",
            "uid": "f410fd76cb91d42866e10d129305b58ea6b32e4b3024bc77ffe1192d2e7caf85"
        },
        "name": "fact",
        "runtime_type": 2,
        "version": "1.0"
        }
    },
    "tx_block_height": 2,
    "tx_id": "172007a1078c256cca0dd1b60e1789adc59f4b0e2968492c9d632a8454c132e0",
    "tx_timestamp": 1666340250
    }
    ```
    执行上面的 `创建 wasm 合约` 命令后，可以看到 mysql 数据库的 `sample_transactions` 表格中添加了两行新内容：
    ```bash
    mysql> select * from sample_transactions;
    +------------------------------------------------------------------+-------------------------+--------+
    | tx_id                                                            | occur_time              | source |
    +------------------------------------------------------------------+-------------------------+--------+
    | 172007a0cb8dc70aca65329e5a4dc518ad59b77971a5455cbac17db2271166de | 2022-10-21 16:17:29.319 |      0 |
    | 172007a1078c256cca0dd1b60e1789adc59f4b0e2968492c9d632a8454c132e0 | 2022-10-21 16:17:30.333 |      0 |
    +------------------------------------------------------------------+-------------------------+--------+
    2 rows in set (0.00 sec)
    ```

    再执行调用合约的命令
    ```bash
    $ ./cmc client contract user invoke \
    --contract-name=fact \
    --method=save \
    --sdk-conf-path=./testdata/sdk_config_solo.yml \
    --params="{\"file_name\":\"name007\",\"file_hash\":\"ab3456df5799b87c77e7f88\",\"time\":\"6543234\"}" \
    --sync-result=true

    # 以下为返回结果
    {
    "contract_result": {
        "contract_event": [
        {
            "contract_name": "fact",
            "contract_version": "1.0",
            "event_data": [
            "ab3456df5799b87c77e7f88",
            "name007",
            "6543234"
            ],
            "topic": "topic_vx",
            "tx_id": "172007b16c772c24ca814ef1351dc5f0f1520ce6f8644866bdd724d4df7c4a2e"
        }
        ],
        "gas_used": 238311
    },
    "tx_block_height": 3,
    "tx_id": "172007b16c772c24ca814ef1351dc5f0f1520ce6f8644866bdd724d4df7c4a2e",
    "tx_timestamp": 1666340320
    }
    ```
    可以看到 table 中又增加了一条 SampleTransaction 记录：
    ```bash
    mysql> select * from sample_transactions;
    +------------------------------------------------------------------+-------------------------+--------+
    | tx_id                                                            | occur_time              | source |
    +------------------------------------------------------------------+-------------------------+--------+
    | 172007a0cb8dc70aca65329e5a4dc518ad59b77971a5455cbac17db2271166de | 2022-10-21 16:17:29.319 |      0 |
    | 172007a1078c256cca0dd1b60e1789adc59f4b0e2968492c9d632a8454c132e0 | 2022-10-21 16:17:30.333 |      0 |
    | 172007b16c772c24ca814ef1351dc5f0f1520ce6f8644866bdd724d4df7c4a2e | 2022-10-21 16:18:40.738 |      0 |
    +------------------------------------------------------------------+-------------------------+--------+
    3 rows in set (0.00 sec)
    ```


## 至此，项目本机测试成功，可以提交代码了。

---
# update1 性能指标测量工作的动态开关
2022.11.03
## 用法说明  
本次更新，新添加了如下5个文件:
```bash
performance_config.go
performance_config.yml
performance_config_server.go
performance_config_server_test.go
Makefile
```

在 `performance_config.go` 文件中，增加了 `PerformanceConfig` 结构的定义，并且声明了包级私有变量 `var perfConfig PerformanceConfig`。  
`perfConfig` 变量中的各个 `field` 默认都是 `0值`（0, false, nil ......），即刚开始启动 `chainmaker-go` 的时候，默认所有的性能指标数据都不需要进行测量。

### 添加性能测量控制开关
还是以前面举例过的 `SampleTransaction` 作为例子进行说明。  
- 对于想要测量的性能指标，在 `model` 目录下定义相应的结构体，如 `type SampleTransaction struct`。  
- 然后在 `performance_config.go` 文件中的 `type PerformanceConfig struct` 结构定义中，添加同名的 `field` :   
    ```go
    type PerformanceConfig struct {
        MeasureAll  bool `yaml:"measure_all"`
        SampleTransaction bool `yaml:"sample_transaction"`
        ...
    }
    ```

- 接着再添加相应的开关函数，读取该 `field` 值：
    ```go
    func SampleTransaction() bool {
        return perfConfig.MeasureAll && perfConfig.SampleTransaction
    }
    ```
    这里之所以使用到了 `perfConfig.MeasureAll`，是为了使用该成员变量作为一个全局开关，如果该变量的值为 `false`，则所有的性能指标都不需要进行测量了。

### 更新性能测量控制开关
- 更新配置  
    `performance` 框架根目录下，有一个 `performance_config.yml` 文件，该文件的内容格式与 `type PerformanceConfig struct` 是对应的：
    ```yaml
    measure_all: true
    sample_transaction: true
    # sample_rate: 10
    ```
    在 `chainmaker-go` 启动过程中，如果成功调用了 `performance.InitWithDNS(dns, listenerPort)` 函数，则会在该函数中调用 `startConfigListener()`，从而启动一个 `http server`，倾听的端口即为 `listenerPort`，该值就是在 `chainmaker.yml` 中配置的`perf_config_port`。  
    > 如果在同一台机器上启动多个node，需要将不同的node的`perf_config_port`配置为不同的值，避免端口冲突；如果只需要单独一个node做性能指标的测量工作，可以将不需要进行测量工作的node的`chainmaker.yml`中的`plugin`配置项注释掉。  

    可以使用如下两个命令来查看或更新 `perfConfig` 的值：  
    ```bash
    ### 查看 perfConfig
    curl localhost:9527/perfconfig/allconfigs

    ### 根据 performance_config.yml 的值更新 perfConfig
    curl -XPOST --data-binary @performance_config.yml localhost:9527/perfconfig/update
    ```
    > 注意，如果 `chainmaker-go` 不是在本机运行的，上面两个命令中的 `localhost` 要替换成 remote machine 的 ip。

    为了方便用户使用，在 `performance` 项目根目录下新添加了一个 `Makefile`，其内容如下：
    ```makefile
    perfConfigPort=9527

    allconfigs:
        @curl localhost:${perfConfigPort}/perfconfig/allconfigs
    update:
        @curl -XPOST \
        --data-binary @performance_config.yml \
        localhost:${perfConfigPort}/perfconfig/update

    ```
    在 `performance` 根目录下，直接修改`performance_config.yml`文件成期望的配置，然后执行 `make allconfigs` 或者 `make update` 命令，即可查询、更新 `performance config`：
    ```bash
    [performance]$ make allconfigs
    measure_all: false
    sample_transaction: false
    sample_rate: 0

    [performance]$ make update
    measure_all: true
    sample_transaction: false
    sample_rate: 0
    ```

- 只更新 `perfConfig` 中的单个字段  
    `performance_config.yml` 中只填写想要更新的字段
    ```yaml
    sample_transaction: true
    ```
    然后执行更新命令即可：
    ```bash
    [performance]$ curl localhost:9527/perfconfig/allconfigs:95
    measure_all: true
    sample_transaction: false
    sample_rate: 0

    [performance]$ curl -XPOST --data-binary @performance_config.yml localhost:9527/perfconfig/update
    measure_all: true
    sample_transaction: true # 只更新了目标配置项
    sample_rate: 0
    ```


- 测试 `/perfconfig/allconfigs` 和 `/perfconfig/update` 两个 `endpoint`  
  - 在 `performance` 根目录下，使用如下命令启动 `config server`:
    ```bash
    go test -v -run=TestConfigServer
    ```
  - 切换到一个新的 `terminal` 窗口，查看并更新 `perfConfig`:
    ```bash
    [performance]$  curl localhost:9527/perfconfig/allconfigs
    measure_all: false
    sample_transaction: false
    sample_rate: 0

    [performance]$ curl -XPOST --data-binary @performance_config.yml localhost:9527/perfconfig/update
    measure_all: true
    sample_transaction: false
    sample_rate: 0
    ```




### 使用性能测量控制开关
- 在引用 `performance` 框架的代码中，可以根据相应的`开关函数`来决定是否进行性能指标的测量：
    ```go
    // file: txpool-normal/normal_tx_pool.go
    import (
        ......
        "chainmaker.org/chainmaker/performance"
        "chainmaker.org/chainmaker/performance/model"
    )

    func (pool *normalPool) AddTx(tx *commonPb.SampleTransaction, source protocol.TxSource) error {
        ......
        if performance.SampleTransaction() { // 这样就实现了开关的效果
            transaction := &model.SampleTransaction{
                TxID:      tx.Payload.GetTxId(),
                OccurTime: time.Now(),
                Source:    int(source),
            }
            performance.Record(transaction, nil)
	    }

        ......
        return nil
    }
    ```