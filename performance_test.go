package performance

import (
	"strconv"
	"testing"

	"time"

	"chainmaker.org/chainmaker/performance/model"
)

var dns = "chainmaker:chainmaker@tcp(127.0.0.1:3306)/chainmaker_performance?charset=utf8mb4&parseTime=True&loc=Local"

var (
	signal chan error
)

func TestMain(m *testing.M) {
	signal = make(chan error, 1)
	InitWithDNS(dns, 9527)
	m.Run()
	close(signal)
}

func testError(t *testing.T, signal <-chan error, wantError bool) {
	err := <-signal
	if (err != nil) != wantError {
		t.Helper()
		t.Errorf("wantError:%t, but got err: %v", wantError, err)
	}
}

func TestSampleTransaction(t *testing.T) {
	transaction := &model.SampleTransaction{
		TxID:      strconv.Itoa(int(time.Now().UnixNano())),
		OccurTime: time.Now().Add(-time.Hour * 13),
		Source:    1,
	}
	Record(transaction, signal)
	testError(t, signal, false)

	transaction.TxID = strconv.Itoa(int(time.Now().UnixNano()))
	transaction.OccurTime = time.Now()
	Record(transaction, signal)
	testError(t, signal, false)

	Record(transaction, signal)
	testError(t, signal, true) // here we are supposed to get an error

}
