package performance

import (
	"encoding/json"
	"fmt"
	"reflect"

	"chainmaker.org/chainmaker/performance/model"
	"chainmaker.org/chainmaker/performance/util"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var (
	dbConnected = false
	db          *gorm.DB
)

/*
InitWithDNS 初始化 package performance, dns 是需要连接到的msyql地址

	格式为："user:passwd@tcp(ip:port)/database_name?charset=utf8mb4&parseTime=True&loc=Local"
	例如：
	dns = "chainmaker:chainmaker@tcp(127.0.0.1:3306)/chainmaker_performance?charset=utf8mb4&parseTime=True&loc=Local"
	listenerPort 是用于动态更新 perfConfig 的后门端口
*/
func InitWithDNS(dns string, listenerPort uint16) error {
	var err error
	db, err = gorm.Open(mysql.Open(dns), &gorm.Config{})
	if err != nil {
		return fmt.Errorf("connect to dns(%s) failed: %s", dns, err.Error())
	}
	dbConnected = true
	initAllModels()
	startConfigListener(listenerPort)
	return nil
}

/*
Record 将 value 插入数据库；使用异步方式，不会阻塞 caller；如果 caller 需要确认执行是否完成，可以传入 sig chan error，用于接收执行结果
*/
func Record(value interface{}, sig chan<- error) {
	if dbConnected {
		go util.SafeGoroutine(func() error {
			result := db.Create(value)
			if result.Error != nil {
				name := reflect.TypeOf(value).Name()
				data, _ := json.Marshal(value)
				return fmt.Errorf("Record(%s), data=%s, got err: %s", name, data, result.Error.Error())
			}
			return nil
		}, sig)
	} else if sig != nil {
		sig <- fmt.Errorf("[performance] database not connected")
	}
}

func initAllModels() {
	for _, model := range model.AllModels {
		if !db.Migrator().HasTable(model) {
			err := db.Migrator().CreateTable(model)
			if err != nil {
				fmt.Printf("CreateTable(%s) failed\n", reflect.TypeOf(model).Elem().Name())
			} else {
				fmt.Printf("CreateTable(%s) succeded\n", reflect.TypeOf(model).Elem().Name())
			}
		}
	}
	model.AllModels = nil // leave the underlying memory to gc
}
