module chainmaker.org/chainmaker/performance

go 1.16

require (
	github.com/gin-gonic/gin v1.7.4
	gorm.io/driver/mysql v1.4.3
	gorm.io/gorm v1.24.0
)
